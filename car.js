import { Vehicle } from "./vehicle.js";

class Car extends Vehicle {
    constructor(brand, yearManufactured, vId, modelName) {
        super(brand, yearManufactured);
        this.vId = vId
        this.modelName = modelName
    }
    honk() {
        return Car
    }
}
export { Car }