import { Vehicle } from "./vehicle.js";

class Bike extends Vehicle {
    constructor(brand, yearManufactured, vId, modelName) {
        super(brand, yearManufactured);
        this.vId = vId
        this.modelName = modelName
    }
    honk() {
        return Bike
    }
}
export { Bike }
