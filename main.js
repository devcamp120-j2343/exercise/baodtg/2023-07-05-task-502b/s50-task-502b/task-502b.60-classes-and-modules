import { Vehicle } from "./vehicle.js";
import { Car } from "./car.js";
import { Bike } from "./bike.js";


var car = new Vehicle("Honda", 2023);
console.log(car.print())
console.log(car.brand)
console.log(car.yearManufactured);
console.log(car instanceof Vehicle);

var car2 = new Car("Toyota", 2022, 23, "SUV")
console.log(car2.brand);
console.log(car2.yearManufactured);
console.log(car2.vId);
console.log(car2.modelName);
console.log(car2.honk());

var bike1 = new Bike("Martin", 2008, 1, "@");
console.log(bike1.brand);
console.log(bike1.yearManufactured);
console.log(bike1.vId);
console.log(bike1.modelName);
console.log(bike1.honk());
console.log(bike1 instanceof Bike, bike1 instanceof Car)